import {defineConfig} from 'powerful-cli';
import config from 'powerful-ui/exports/powerful.config';
import path from 'node:path';

export default defineConfig<CONFIG.Extra>(async (
	{
		returnCheck,
		baseDir,
		packageJson,
		args,
		command,
		utils:{
			extend,
			copyReplace,
			copyCover,
			copyCustom,
			dateFormat,
		},
	},
) => {
	const context:CONFIG.Extra['context'] = {
		platform:(args.platform ?? 'pc') as CONFIG.Extra['context']['platform'],
	};
	return returnCheck({
		extendConfigs:[
			config,
		],
		extraEnv:{
			...context,
		},
		context,
	});
}, async (
	{
		extendConfigs,
		envPath,
		extraEnv,
		context,
		returnCheck,
		baseDir,
		packageJson,
		args,
		command,
		utils:{
			extend,
			copyReplace,
			copyCover,
			copyCustom,
			dateFormat,
		},
	},
) => {
	return returnCheck({
		alias:{
			'#':path.join(baseDir, 'src', context.platform),
		},
		buildFile:{
			emit:command !== 'serve',
			name:command === 'lib'
				? 'lib/build-info.txt'
				: 'build-info.txt',
			content:[
				`构建平台: ${context.platform}`,
			],
		},
		typescript:{
			active:true,
			configFile:path.join(baseDir, 'src', context.platform, 'tsconfig.json'),
		},
	});
});