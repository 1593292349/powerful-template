import 'powerful-cli/types';
import 'powerful-ui/types';

declare global{
	namespace CONFIG{
		type Extra = {
			context:{
				platform:'pc' | 'mobile';
			};
		};
	}
}
export {};