import Vue, {
	h,
} from 'vue';
import router from '#/router';
import pinia from '#/store';
import '#/style/global.scss';

Vue.config.productionTip = false;
Vue.config.performance = process.env.NODE_ENV === 'development';

new Vue({
	router,
	pinia,
	setup(){
		return () => {
			return h('router-view');
		};
	},
}).$mount('#app');