import {
	setLocale,
	languages,
} from 'powerful-ui/exports/pc-default';

setLocale(languages.zhCn);

export * from 'powerful-ui/exports/pc-default';