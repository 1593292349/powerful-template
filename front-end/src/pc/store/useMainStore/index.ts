import {
	shallowRef,
} from 'vue';
import {
	defineStore,
} from 'pinia';

export default defineStore('main', () => {
	const globalData = shallowRef('全局数据');
	return {
		globalData,
	};
});