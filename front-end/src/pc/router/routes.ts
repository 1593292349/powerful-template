import {
	util,
} from '#/powerful';

export default util.getRoutes([
	{
		name:'主框架',
		path:'',
		component:() => import('#/views/App.vue'),
	},
]);