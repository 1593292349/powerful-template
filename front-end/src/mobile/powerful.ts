import {
	setLocale,
	languages,
} from 'powerful-ui/exports/mobile-default';

setLocale(languages.zhCn);

export * from 'powerful-ui/exports/mobile-default';