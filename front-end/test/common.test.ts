import lib from '@/pc/lib';
const {add} = lib;

test('add', () => {
	expect(add(1, 2)).toEqual(3);
});